import copy
import json
from os.path import join, expanduser
import yaml

class ConfigAgent():
    """
    Class to configure the initial values of the meili_agent
    """

    def __init__(self ):
        # config agent by reading and storing config files about
        # number of vehicles, prefixes and topics
        self.cfg_file_path = join(expanduser("~"), ".meili", "cfg.yaml")
        self.file_name_vehicles = join(expanduser("~"), ".meili", "config.yaml")

        self.data = None
        self.topics = None
        self.cfg = None

    def check_empty(self, variable, name):
        if not variable:
            print("[Config] %s is empty" % (name,))
            raise AssertionError("[Config] Empty", name)

    def open_yaml(self, filename):
        try:
            with open(filename, "r", encoding="utf-8") as config:
                return yaml.safe_load(config)
        except OSError:
            print("[Config] %s does not exist" % (filename,))
            raise

    def open_json(self, filename):
        try:
            with open(filename, "r", encoding="utf-8") as t:
                return json.load(t)
        except OSError:
            print("[Config] %s does not exist" % (filename,))
            raise


    def open_files(self):
        # Parse Mode, number of vehicles and tokens from config files passed as arguments

        self.data = self.open_yaml(self.file_name_vehicles)
        self.cfg = self.open_yaml(self.cfg_file_path)

        self.check_empty(self.data, "Data file")
        self.check_empty(self.cfg, "Config file")

    def config_var(self):
        try:
            setup_token = self.cfg["token"]
            server_instance = self.cfg["site"]
            token = self.data["fleet"]

            fleet = True
        except KeyError as e:
            print(
                "[Config] Configuration of variable %s error" % (e,)
            )
            raise

        self.check_empty(setup_token, "Config file, token")
        self.check_empty(server_instance, "Config file, server_instance")
        self.check_empty(token, "Data file, fleet")

        return setup_token, server_instance, token, fleet

    def config_vehicles(self):

        self.check_empty(self.data["vehicles"], "Data file, vehicles")
        vehicles=self.data["vehicles"]
        vehicle_list = []
        vehicle_tokens = []
        vehicle_names = []

        try:
            # creating sublist of vehicles and tokens
            for vehicle in vehicles:
                vehicle_list.append(vehicle["uuid"])
                vehicle_tokens.append(vehicle["token"])
                vehicle_name = vehicle["token"].split(" ")[0]
                vehicle_names.append(vehicle_name)

        except KeyError as error:
            self.print(
                "[Config] Configuration of variable %s error" % (error,)
            )
            raise

        return vehicle_list, vehicles, vehicle_tokens, vehicle_names
