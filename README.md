# Python Simulator

This repository allows you to simulate multiple robots so that they appear in the FMS fleet.

Currently, it supports:

- [x] Autoconfiguration of the simulator
- [x] Send the locations of multiple robots to the FMS
- [x] Receive "move to point" tasks from the FMS
- [x] Simulate the movement of robots
- [x] Update the task status according to the simulation
- [x] Task cancellation
- [x] Traffic control

# Autoconfiguration

A GUI is provided to easily configure the FMS and simulator. The GUI will generate the necessary configuration files for the simulator.

## Steps

1. Download the suitable version of [meili-cli](https://docs.meilirobots.com/docs/get-started/on-robot-agent/meili-cli/#download) into the same directory as the simulator.

2. Run the following command in a terminal:

    ```bash
    python3 gui.py
    ```

3. Fill in the necessary information in the GUI and click 'Generate'. **There is an option to save the input data for future use. However, beware that it is not secure, as the data is saved in a JSON.**


4. Check the output in the terminal for possible errors. If there are no errors, the configuration files will be generated in the '~/.meili/' directory.

# Usage

The files 'config.yaml' and 'cfg.yaml' generated with meili-cli should be placed in '~/.meili/'.

In a terminal, run the following command:

```bash
python3 main.py
```

# Configuration

Vehicles will spawn randomly inside the rectangular area defined by the constants in 'simulated_vehicle.py'. Modify these constants to change the spawn area.
