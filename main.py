import time
from config_agent import ConfigAgent
from meili_sdk_handler import MeiliSdkHandler
from simulated_vehicle import SimulatedVehicle

from meili_sdk.exceptions import ValidationError
from meili_sdk.websockets import constants
from meili_sdk.websockets.models.message import Message

if  __name__ == "__main__":
    print("Initiating Python Simulator")

    # Read configuration file
    print("Reading configuration files")
    config_agent = ConfigAgent()
    config_agent.open_files()
    
    setup_token, server_instance, token, fleet = config_agent.config_var()
    vehicle_list, vehicles, vehicle_tokens, vehicle_names = config_agent.config_vehicles()

    # Create a simulated robot for each vehicle
    print("Creating simulated robots for each vehicle")
    simulated_vehicles = []
    for vehicle_uuid, vehicle_name in zip(vehicle_list, vehicle_names):
        vehicle = SimulatedVehicle(vehicle_uuid, vehicle_name)
        simulated_vehicles.append(vehicle)
        print(f"Simulated robot {vehicle} created")

    # Create connection to FMS with Meili-SDK
    print("Creating connection to FMS with Meili-SDK")
    meili_sdk_handler = MeiliSdkHandler(token, server_instance, vehicle_list, simulated_vehicles)
    for vehicle in simulated_vehicles:
        vehicle.set_meili_sdk_handler(meili_sdk_handler)

    while True:
        time.sleep(1)

