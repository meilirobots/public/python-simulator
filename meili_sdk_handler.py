from meili_sdk.websockets.client import MeiliWebsocketClient
from meili_sdk.websockets.models.task_v2 import TaskV2

class MeiliSdkHandler:
    def __init__(self, token, server_instance, vehicle_list, simulated_vehicles):
        self.client = MeiliWebsocketClient(
            token=token,
            override_host=server_instance.replace("http", "ws"),
            open_handler=self.open_handler,
            close_handler=self.close_handler,
            error_handler=self.error_handler,
            task_v2_handler=self.task_v2_handler,
            task_cancellation_handler=self.task_cancellation_handler,
            slow_down_handler=self.slow_down_handler,
            collision_clearance_handler=self.collision_clearance_handler,
            update_map_handler=self.update_map_handler,
            pause_task_handler=self.pause_task_handler,
            resume_task_handler=self.resume_task_handler,
        )

        self.vehicle_list = vehicle_list
        for vehicle in vehicle_list:
            self.client.add_vehicle(vehicle)

        self.simulated_vehicles = simulated_vehicles

        self.client.run_in_thread()

    @staticmethod
    def close_handler():
        print("Disconnected to Meili WS")

    @staticmethod
    def open_handler():
        print("Connected to Meili WS")

    @staticmethod
    def error_handler(_, err):
        print(f"WS error: {err}")

    def task_v2_handler(self, task: TaskV2, vehicle_uuid: str):
        # Find the vehicle corresponding to the vehicle_uuid
        target_vehicle = None
        for vehicle in self.simulated_vehicles:
            if vehicle.get_uuid() == vehicle_uuid:
                target_vehicle = vehicle
                break

        if target_vehicle is None:
            print(f"Vehicle {vehicle_uuid} not found")
            return

        print(f"Received task for vehicle {target_vehicle}")

        if target_vehicle.set_task(task):
            target_vehicle.start_task()


    def task_cancellation_handler(self, data: dict, vehicle_uuid: str) -> None:
        if vehicle_uuid not in self.vehicle_list:
            print(f"Vehicle {vehicle_uuid} not found")
            return
        
        for vehicle in self.simulated_vehicles:
            if vehicle.get_uuid() == vehicle_uuid:
                vehicle.cancel_task()
                break

    def slow_down_handler(self, slow_down_msg, data: dict, vehicle_uuid: str):
        if vehicle_uuid not in self.vehicle_list:
            print(f"Vehicle {vehicle_uuid} not found")
            return
        
        for vehicle in self.simulated_vehicles:
            if vehicle.get_uuid() == vehicle_uuid:
                vehicle.pause_task()
                break    

    def collision_clearance_handler(
        self, collision_clearance_msg, data: dict, vehicle_uuid: str
    ):
        if vehicle_uuid not in self.vehicle_list:
            print(f"Vehicle {vehicle_uuid} not found")
            return
        
        for vehicle in self.simulated_vehicles:
            if vehicle.get_uuid() == vehicle_uuid:
                vehicle.resume_task()
                break

    def update_map_handler(self, data, vehicle, status):
        pass

    def pause_task_handler(self, data, vehicle_uuid):
        if vehicle_uuid not in self.vehicle_list:
            print(f"Vehicle {vehicle_uuid} not found")
            return
        
        for vehicle in self.simulated_vehicles:
            if vehicle.get_uuid() == vehicle_uuid:
                vehicle.pause_task()
                break

    def resume_task_handler(self, data, vehicle_uuid):
        if vehicle_uuid not in self.vehicle_list:
            print(f"Vehicle {vehicle_uuid} not found")
            return
        
        for vehicle in self.simulated_vehicles:
            if vehicle.get_uuid() == vehicle_uuid:
                vehicle.resume_task()
                break