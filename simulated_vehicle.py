import time
import threading
import random

from meili_sdk.websockets import constants
from meili_sdk.websockets.models.message import Message
from meili_sdk.exceptions import ValidationError

# Modify the following constants to change where the robots can spawn
MIN_X = 0
MAX_X = 10
MIN_Y = -5
MAX_Y = 0

class TaskStatus:
    IN_PROGRESS = 1
    COMPLETED = 3
    ABORTED = 4

class SimulatedVehicle:
    supported_actions = ["move_to_point"]

    def __init__(self, vehicle_uuid, vehicle_name):
        self.vehicle_uuid = vehicle_uuid
        self.vehicle_name = vehicle_name
        self.position = {"x": random.uniform(MIN_X, MAX_X), "y": random.uniform(MIN_Y, MAX_Y), "rotation": 0}
        self.task = None
        self.waypoints = []
        self.move_to_waypoints_thread = None
        self._cancel_event = threading.Event()
        self._pause_event = threading.Event()

        self.send_status_to_fms_thread = threading.Thread(target=self.send_status_to_fms)

        self.meili_sdk_handler = None

    def send_status_to_fms(self):
        while True:
            status_message = self.get_robot_status()
            message = Message(
                event=constants.EVENT_STATUS,
                value=status_message,
                vehicle=self.get_uuid(),
            )
            try:
                message.validate()
            except ValidationError as e:
                print(f"Robot {self} status message validation error: {e}")
            else:
                self.meili_sdk_handler.client.send(message)
            time.sleep(1)

    def set_meili_sdk_handler(self, meili_sdk_handler):
        self.meili_sdk_handler = meili_sdk_handler
        self.send_status_to_fms_thread.start()

    def set_task(self, task):
        if task.action.action_type not in self.supported_actions:
            print(f"Action {task.action.action_type} not supported, skipping task {task.uuid}")
            self.task_status_update(TaskStatus.COMPLETED, task.uuid)
            return False
        
        self.task = task
        self.process_waypoints(task)
        return True

    def start_task(self):
        if self.task is not None:
            self.move_to_waypoints_thread = threading.Thread(target=self.move_to_waypoints)
            self.move_to_waypoints_thread.start()

    def cancel_task(self):
        if self.move_to_waypoints_thread is not None and self.move_to_waypoints_thread.is_alive():
            print(f"Canceling task {self.task.uuid} for vehicle {self}")
            self._cancel_event.set()
            self.move_to_waypoints_thread.join()
            print(f"Task {self.task.uuid} canceled for vehicle {self}")

        self.task = None
        self.waypoints = []

    def pause_task(self):
        if self.move_to_waypoints_thread is not None and self.move_to_waypoints_thread.is_alive():
            print(f"Pausing task {self.task.uuid} for vehicle {self}")
            self._pause_event.set()

    def resume_task(self):
        if self.move_to_waypoints_thread is not None and self.move_to_waypoints_thread.is_alive():
            print(f"Resuming task {self.task.uuid} for vehicle {self}")
            self._pause_event.clear()

    def process_waypoints(self, task):
        try:
            if task.metric_waypoints != []:
                self.waypoints = task.metric_waypoints
            else:
                self.waypoints = [[task.action.point["metric"]["x"], task.action.point["metric"]["y"]]]
            self.rotations = task.rotation_angles
            # add rotations to waypoints
            for i, rotation in enumerate(self.rotations):
                self.waypoints[i].append(rotation)
        except AttributeError as e:
            print(f"Task {task.uuid} does not have attribute: {e}")

    def move_to_waypoints(self):
        Kp = 0.1
        max_speed = 0.05

        print(f"[WP Thread] Task {self.task.uuid} started for vehicle {self}")

        self.task_status_update(TaskStatus.IN_PROGRESS)

        for waypoint in self.waypoints:
            while True:
                # Check if the task has been cancelled
                if self._cancel_event.is_set():
                    print(f"[WP Thread] Task {self.task.uuid} cancelled for vehicle {self}")
                    self.task_status_update(TaskStatus.ABORTED)
                    self._cancel_event.clear()
                    return  # Exit the method to stop the thread
                
                # Check if the task has been paused
                if self._pause_event.is_set():
                    time.sleep(1)
                    continue

                x = self.position["x"]
                y = self.position["y"]

                dx = float(waypoint[0]) - x
                dy = float(waypoint[1]) - y

                distance = (dx ** 2 + dy ** 2) ** 0.5

                if distance < 0.1:
                    break

                # Normalize the direction vector
                direction_x = dx / distance
                direction_y = dy / distance

                # Calculate the new position
                move_x = direction_x * min(max_speed, Kp * distance)
                move_y = direction_y * min(max_speed, Kp * distance)

                # Update the position
                self.position["x"] += move_x
                self.position["y"] += move_y

                time.sleep(0.1)

            # Rotation update (optional to do it continuously or at the end)
            self.position["rotation"] = float(waypoint[2])

        self.task_status_update(TaskStatus.COMPLETED)
        print(f"[WP Thread] Task {self.task.uuid} completed for vehicle {self}")

    def task_status_update(self, task_status, task_uuid=None):
        if task_uuid is None:
            task_uuid = self.task.uuid

        value = {"goal_id": self.task.uuid, "status_id": task_status}
        
        message = Message(event=constants.EVENT_GOAL_STATUS, value=value, vehicle=self.vehicle_uuid)
        try:
            message.validate()
        except ValidationError as e:
            print(f"Robot {self} status message validation error: {e}")
        else:
            self.meili_sdk_handler.client.send(message)                              

    def get_uuid(self):
        return self.vehicle_uuid

    def get_position(self):
        return self.position
    
    def get_robot_status(self):
        return {
            "timestamp": time.time(),
            "location": {"xm": self.position["x"], "ym": self.position["y"], "rotation": self.position["rotation"]},
            "battery": {
                "value": 100,
                "power_supply_status": 3,
            },
            "speed": {"speed": 0},
            "state_text": "",
            "map_id": "",
            "current_mission_id": ""
        }

    def __str__(self):
        return self.vehicle_name                
