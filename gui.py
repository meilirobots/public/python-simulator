import subprocess
import tkinter as tk
import tkinter.messagebox
import requests
import json
import sys
from datetime import datetime
import os
import time

CONFIG_FILE = "config.json"


class ConfigManager:
    """Handles saving and loading configuration data."""
    
    @staticmethod
    def load_config():
        if os.path.exists(CONFIG_FILE):
            with open(CONFIG_FILE, "r") as f:
                return json.load(f)
        return {"login": "", "password": "", "organization": "", "team": "", "num_vehicles": 1, "access_token": ""}

    @staticmethod
    def save_config(config):
        with open(CONFIG_FILE, "w") as f:
            json.dump(config, f)


class SimulatorApp:
    """Handles the GUI and user input."""
    
    def __init__(self, root):
        self.root = root
        self.config = ConfigManager.load_config()  # Load last configuration
        self.sim_configuration_service = SimulatorConfigurationService()

        self.create_widgets()
    
    def create_widgets(self):
        self.root.title("Simulator GUI")

        # Server URL
        tk.Label(self.root, text="Server URL:").grid(row=0, column=0, padx=10, pady=5)
        self.server_url_dropdown = tk.StringVar()
        self.server_url_dropdown.set("http://localhost:8000")
        tk.OptionMenu(self.root, self.server_url_dropdown, "http://localhost:8000", "https://development.meilirobots.com", "https://app.meilirobots.com").grid(row=0, column=1, padx=10, pady=5)

        # Login
        tk.Label(self.root, text="Login:").grid(row=1, column=0, padx=10, pady=5)
        self.login_entry = tk.Entry(self.root)
        self.login_entry.grid(row=1, column=1, padx=10, pady=5)
        self.login_entry.insert(0, self.config["login"])

        # Password
        tk.Label(self.root, text="Password:").grid(row=2, column=0, padx=10, pady=5)
        self.password_entry = tk.Entry(self.root, show="*")
        self.password_entry.grid(row=2, column=1, padx=10, pady=5)
        self.password_entry.insert(0, self.config["password"])

        # Organization
        tk.Label(self.root, text="Organization:").grid(row=3, column=0, padx=10, pady=5)
        self.org_entry = tk.Entry(self.root)
        self.org_entry.grid(row=3, column=1, padx=10, pady=5)
        self.org_entry.insert(0, self.config["organization"])

        # Team Name
        tk.Label(self.root, text="Team Name:").grid(row=4, column=0, padx=10, pady=5)
        self.team_entry = tk.Entry(self.root)
        self.team_entry.grid(row=4, column=1, padx=10, pady=5)
        self.team_entry.insert(0, self.config["team"])

        # Number of Vehicles
        tk.Label(self.root, text="Number of Vehicles:").grid(row=5, column=0, padx=10, pady=5)
        self.num_vehicles_spinbox = tk.Spinbox(self.root, from_=1, to=10)
        self.num_vehicles_spinbox.grid(row=5, column=1, padx=10, pady=5)
        self.num_vehicles_spinbox.delete(0, "end")
        self.num_vehicles_spinbox.insert(0, self.config["num_vehicles"])

        # Access Token
        tk.Label(self.root, text="Access Token:").grid(row=6, column=0, padx=10, pady=5)
        self.access_token_entry = tk.Entry(self.root)
        self.access_token_entry.grid(row=6, column=1, padx=10, pady=5)
        self.access_token_entry.insert(0, self.config["access_token"])

        # Save configuration checkbox
        self.save_config_var = tk.IntVar()
        self.save_config_checkbox = tk.Checkbutton(self.root, text="Save Configuration (not secure)", variable=self.save_config_var)
        self.save_config_checkbox.grid(row=7, column=0, columnspan=2, padx=10, pady=5)

        # Generate Button
        generate_button = tk.Button(self.root, text="Generate", command=self.generate)
        generate_button.grid(row=8, column=0, columnspan=2, padx=10, pady=5)

    def generate(self):
        server_url = self.server_url_dropdown.get()
        login = self.login_entry.get()
        password = self.password_entry.get()
        organization = self.org_entry.get().lower()
        team_name = self.team_entry.get().lower()
        num_vehicles = int(self.num_vehicles_spinbox.get())
        access_token = self.access_token_entry.get()

        if self.save_config_var.get():
            # Save the current configuration
            self.config.update({"login": login, "password": password, "organization": organization, "team": team_name, "num_vehicles": num_vehicles, "access_token": access_token})
            ConfigManager.save_config(self.config)

        # Set the environment variable for the access token
        os.environ['ACCESS_TOKEN'] = access_token

        # Perform the operations using SimulatorConfigurationService
        self.sim_configuration_service.run_simulation_config(server_url, login, password, organization, team_name, num_vehicles)

        # Close the GUI window
        self.root.destroy()


class SimulatorConfigurationService:
    """Handles API calls and backend operations."""
    
    def authenticate(self, login, password):
        print("[INFO] Authenticating...")
        payload = json.dumps({"email": login, "password": password})
        url = f"{self.host}/api/auth/login/"
        try:
            response = requests.post(url, headers={"Content-Type": "application/json"}, data=payload)
        except requests.exceptions.ConnectionError:
            print(f"[ERROR] Connection failed to {self.host}")
            tkinter.messagebox.showerror("Connection Error", f"Failed to connect to {self.host}")
            sys.exit(1)
        
        if response.status_code not in [200, 201]:
            print(f"[ERROR] Authorization failed: {response.text}")
            sys.exit(1)
        
        token = json.loads(response.text)["token"]
        print(f"[SUCCESS] Authorization successful")
        return {"Content-Type": "application/json", "Authorization": f"Bearer {token}"}
    
    def get_team_uuid(self, headers, team_name):
        print(f"[INFO] Fetching team UUID for team: {team_name}")
        url = f"{self.host}/api/teams/{team_name}"
        response = requests.get(url, headers=headers)
        
        if response.status_code not in [200, 201]:
            print(f"[ERROR] Team not found: {response.text}")
            sys.exit(1)
        
        team_uuid = json.loads(response.text)["uuid"]
        print(f"[SUCCESS] Team UUID: {team_uuid}")
        return team_uuid
    
    def create_vehicles(self, headers, organization, team_uuid, team_name, num_vehicles):
        vehicles_uuids = []
        for i in range(num_vehicles):
            vehicle_name = f"Sim_{team_name}_{i}"
            payload = {
                "vehicle": {},
                "verbose_name": vehicle_name,
                "team": team_uuid,
                "integration_type": "Wewo",
                "vehicle_type": "amr",
                "active": True
            }

            print(f"[INFO] Checking if organization {organization} exists")
            url = f"{self.host}/api/organizations/{organization}"
            response = requests.get(url, headers=headers)

            if response.status_code != 200:
                print(f"[ERROR] Organization not found: {response.text}")
                tkinter.messagebox.showerror("Organization Not Found", f"Organization {organization} not found")
                sys.exit(1)

            print(f"[INFO] Creating vehicle: {vehicle_name}")
            url = f"{self.host}/api/organizations/{organization}/vehicles/"
            response = requests.post(url, headers=headers, data=json.dumps(payload))
            
            if response.status_code not in [200, 201]:
                # If we receive {"errors":{"verbose_name":["Verbose name is already used"]},"detail":"verbose_name: Verbose name is already used"}
                if "Verbose name is already used" in response.text:
                    print(f"[INFO] Vehicle {vehicle_name} already exists, fetching its UUID.")
                    response = requests.get(url, headers=headers)
                    response = json.loads(response.text)
                    for vehicle in response["results"]:
                        if vehicle["verbose_name"] == vehicle_name:
                            vehicles_uuids.append(vehicle["uuid"])
                            print(f"[SUCCESS] Vehicle UUID: {vehicle['uuid']}")
                            break
                else:
                    print(f"[ERROR] Vehicle creation failed: {response.text}")
                    tkinter.messagebox.showerror("Vehicle Creation Error", f"Failed to create vehicle: {response.text}")
                    sys.exit(1)
            else:
                vehicles_uuids.append(json.loads(response.text)["uuid"])
                print(f"[SUCCESS] Created vehicle with UUID: {vehicles_uuids[-1]}")
        
        return vehicles_uuids
    
    def create_fleet(self, headers, organization, team_uuid, vehicles_uuids):
        print(f"[INFO] Creating fleet with {len(vehicles_uuids)} vehicles")
        payload = {
            "verbose_name": f"Sim_Fleet_{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}",
            "communication_protocol": "websocket",
            "team": team_uuid,
            "communication_format": "json",
            "vehicles": [{"uuid": v} for v in vehicles_uuids]
        }
        
        url = f"{self.host}/api/organizations/{organization}/ros/setups/"
        response = requests.post(url, headers=headers, data=json.dumps(payload))
        
        if response.status_code in [200, 201]:
            print(f"[SUCCESS] Fleet created successfully")
        else:
            print(f"[ERROR] Fleet creation failed: {response.text}")

        fleet_pin = json.loads(response.text)["pin"]
        print(f"[INFO] Fleet PIN: {fleet_pin}")
        return fleet_pin

    def run_meili_cli_setup(self, pin):
        # Set the environment variables using os.environ
        os.environ['PACKAGE_PATH'] = '~/ws'
        os.environ['MEILI_HOST_OVERRIDE'] = self.host

        print(f"PACKAGE_PATH: {os.environ['PACKAGE_PATH']}")
        print(f"MEILI_HOST_OVERRIDE: {os.environ['MEILI_HOST_OVERRIDE']}")
        print(f"ACCESS_TOKEN: {os.environ['ACCESS_TOKEN']}")

        # Initialize the meili-cli using subprocess
        subprocess.run(f"./meili-cli init -pin {pin}", shell=True)

        # Predefined inputs to send to the interactive setup command
        # These inputs will be sent to the command when it asks for them.
        inputs = "\n" 

        # Run the meili-cli setup command and send inputs (due to WEWO configuration we wont use)
        process = subprocess.Popen(
            "./meili-cli setup", 
            shell=True, 
            stdin=subprocess.PIPE, 
            stdout=subprocess.PIPE, 
            stderr=subprocess.PIPE,
            text=True  # Ensure text mode for stdin/stdout
        )

        # Send inputs to the process and wait for it to finish
        process.communicate(inputs)

        # Remove folder created by meili-cli
        subprocess.run("rm -rf ws", shell=True)
    
    def run_simulation_config(self, server, login, password, organization, team_name, num_vehicles):
        self.host = server
        headers = self.authenticate(login, password)
        team_uuid = self.get_team_uuid(headers, team_name)
        vehicles_uuids = self.create_vehicles(headers, organization, team_uuid, team_name, num_vehicles)
        pin = self.create_fleet(headers, organization, team_uuid, vehicles_uuids)

        time.sleep(1)

        self.run_meili_cli_setup(pin)


# Run the application
if __name__ == "__main__":
    root = tk.Tk()
    app = SimulatorApp(root)
    root.mainloop()
